package id.fineasy.crm.service.registry;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

import javax.annotation.PostConstruct;
import java.lang.invoke.MethodHandles;
import java.util.TimeZone;

@SpringBootApplication
@EnableEurekaServer
public class ServiceRegistry {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        SpringApplication.run(ServiceRegistry.class, args);
    }

    @PostConstruct
    public void init(){
        // Setting Spring Boot SetTimeZone
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Jakarta"));
        log.info("Timezone was set! "+ DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }
}
